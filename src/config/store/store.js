import Vue from 'vue';
import Vuex from 'vuex';

import { db, storage } from '@/main';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isMenuVisible: false,
        isUserMenuVisible: false,
        hasCreatedItem: false,
        isAuthScreen: false,

        data: [],
        usersData: [],
        newsData: [],
        eventsData: [],
        categoriesData: []
    },
    mutations: {
        toggleMenu(state, isVisible) {
            if(isVisible === undefined) {
                state.isMenuVisible = !state.isMenuVisible;
            } else {
                state.isMenuVisible = isVisible;
            }
        },
        userMenu(state, isVisible) {
            if(isVisible === undefined) {
                state.isUserMenuVisible = !state.isUserMenuVisible;
            } else {
                state.isUserMenuVisible = isVisible;
            }
        },
        setData(state, payload) {
            const dataType = payload.type;
            const data = payload.data;
            const storageRef = storage.ref();

            if(dataType === 'users') {
                state.usersData.splice(0, state.usersData.length);
                state.usersData = [...data];
            } else if(dataType === 'events') {
                data.forEach(d => {
                    storageRef.child(d.img).getDownloadURL().then(url => {
                        d.img = url;
                    })
                    .then(() => {
                        state.eventsData.splice(0, state.eventsData.length);
                        state.eventsData = [...data];
                    })
                    .catch(error => {
                        throw error;
                    });
                });
            } else if(dataType === 'news') {
                data.forEach(d => {
                    storageRef.child(d.img).getDownloadURL().then(url => {
                        d.img = url;
                    })
                    .then(() => {
                        state.newsData.splice(0, state.newsData.length);
                        state.newsData = [...data];
                    })
                    .catch(error => {
                        throw error;
                    });
                });
            } else {
                state.categoriesData.splice(0, state.categoriesData.length);
                state.categoriesData = [...data];
            }
        },
        insertData(state, payload) {
            const dataType = payload.type;
            const data = payload.data;

            if(dataType === 'users') {
                state.usersData.push(data);
            } else if(dataType === 'events') {
                state.eventsData.push(data);
            } else if(dataType === 'news') {
                state.newsData.push(data);
            } else {
                state.categoriesData.push(data);
            }
        }
    },
    actions: {
        setStateData({commit}, type) {
            const data = [];
            db.collection(type).get()
                .then(snapshot => {
                    snapshot.forEach(doc => {
                        data.push(doc.data());
                    });
                })
                .then(() => {
                    const payload = {
                        data,
                        type
                    }
                    commit('setData', payload);
                })
                .catch(error => {
                    throw error;
                });
        },
        setStateDataById({commit}, data) {
            const dataUnique = [];
            db.collection(data.type).where('id', '==', data.id).get()
                .then(snapshot => {
                    snapshot.forEach(doc => {
                        dataUnique.push(doc.data());
                    });
                })
                .then(() => {
                    const payload = {
                        data: dataUnique,
                        type: data.type
                    }
                    commit('setData', payload);
                })
                .catch(error => {
                    throw error;
                });
        },
        insert(state, data) {
            return new Promise((resolve, reject) => {
                try{
                    state.commit('insertData', data);
                    db.collection(data.type).add(data.data)
                        .then(() => {})
                        .catch(error => console.log('Error adding document: ' + error));

                    if(data.type === 'events' || data.type === 'news') {
                        const storageRef = storage.ref();

                        const prefix = data.data.id;
                        const suffix = data.data.date;
                        const child = storageRef.child(`${data.type}-images/${prefix}-${data.type}-${suffix}`);

                        const image = data.data.img;
                        child.putString(image, 'data_url');
                    }

                    resolve();
                } catch(error) {
                    reject(console.log(error));
                }
            });
        },
        update(state, data) {
            return new Promise((resolve, reject) => {
                try {
                    let docId = '';
                    db.collection(data.type).where('id', '==', data.data.id).get()
                        .then(snapshot => {
                            snapshot.forEach(doc => {
                                docId = doc.id;
                            });
                        })
                        .then(() => {
                            if(data.type === 'events' || data.type === 'news') {
                                const storageRef = storage.ref();

                                const prefix = data.data.id;
                                const suffix = data.data.date;
                                const format = `${data.type}-images/${prefix}-${data.type}-${suffix}`;
                                const child = storageRef.child(format);

                                const image = data.data.img;
                                child.putString(image, 'data_url');

                                data.data.img = format;
                            }
                        })
                        .then(() => {
                            db.collection(data.type).doc(docId).update(data.data);
                        })
                        .catch(error => {
                            throw error;
                        });

                    resolve();
                } catch(error) {
                    reject(console.log(error));
                }
            });
        },
        delete(state, data) {
            return new Promise((resolve, reject) => {
                try{
                    let docId = '';
                    db.collection(data.type).where('id', '==', data.id).get()
                        .then(snapshot => {
                            snapshot.forEach(doc => {
                                docId = doc.id;
                            });
                        })
                        .then(() => {
                            db.collection(data.type).doc(docId).delete();
                        })
                        .then(() => {
                            if(data.type === 'events' || data.type === 'news') {
                                const storageRef = storage.ref();
                                const prefix = data.id;
                                const suffix = data.date;
                                storageRef.child(`${data.type}-images/${prefix}-${data.type}-${suffix}`).delete();
                            }
                        })
                        .catch(error => {
                            throw error;
                        });

                    resolve();
                } catch(error) {
                    reject(console.log(error));
                }
            })
        }
    },
    modules: {
    },
    getters: {
        getData: state => dataType => {
            if(dataType === 'users') {
                return state.usersData;
            } else if(dataType === 'events') {
                return state.eventsData;
            } else if(dataType === 'news') {
                return state.newsData;
            } else {
                return state.categoriesData;
            }
        }
    }
})
