const emailRegex = /^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/;

const rules = {
    usernameRules: [
        v => !!v || 'Username is required!',
        v => (v && v.length <= 20) || 'Username must be less than 20 characters.'
    ],
    emailRules: [
        v=> !!v || 'E-mail is required!',
        v => emailRegex.test(v)
            || 'Invalid e-mail!'
    ],
    passwordRules: [
        v => !!v || 'Password is required!',
        v => passwordRegex.test(v) ||
            'Min 6 characters with at least one capital letter, one lower case and one number'
    ],
    confirmPasswordRules: [
        v => !!v || 'Confirm your password!'
    ]
}

export default { rules, emailRegex, passwordRegex };
