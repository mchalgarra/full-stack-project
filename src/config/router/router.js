import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from "@/pages/Home.vue";
import AdminPage from "@/pages/AdminPage.vue";
import Auth from "@/pages/Auth.vue";

import List from "@/pages/List.vue";
import ItemById from "@/pages/ItemById.vue";
import ItemManagement from '@/pages/ItemManagement.vue';
import NewsPanel from '@/pages/NewsPanel.vue';
import EventsPanel from '@/pages/EventsPanel.vue';

import store from "../store/store.js";

Vue.use(VueRouter);

const routes = [
    {
        name: "home",
        path: "/",
        component: Home
    },
    {
        path: "/administration",
        redirect: "/administration/1",
    },
    {
        name: "administration",
        path: "/administration/:tab",
        component: AdminPage,
        meta: { requiresAdmin: true }
    },
    {
        name: "newsManagement",
        path: "/news/management",
        component: ItemManagement,
        meta: { requiresAdmin: true }
    },
    {
        name: "newsManagementById",
        path: "/news/management/:id",
        component: ItemManagement,
        meta: { requiresAdmin: true }
    },
    {
        name: "news",
        path: "/news",
        component: NewsPanel
    },
    {
        name: "newsCategory",
        path: "/news/:categoryId",
        component: List
    },
    {
        name: "newsById",
        path: "/news/:categoryId/:id",
        component: ItemById
    },
    {
        name: "eventManagement",
        path: "/events/management",
        component: ItemManagement,
        meta: { requiresAdmin: true }
    },
    {
        name: "eventManagementById",
        path: "/events/management/:id",
        component: ItemManagement,
        meta: { requiresAdmin: true }
    },
    {
        name: "events",
        path: "/events",
        component: EventsPanel
    },
    {
        name: "eventCategory",
        path: "/events/:categoryId",
        component: List
    },
    {
        name: "eventById",
        path: "/events/:categoryId/:id",
        component: ItemById
    },
    {
        path: "/categories",
        redirect: "/"
    },
    {
        name: "auth",
        path: "/auth",
        component: Auth
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    const path = to.path;
    const user = JSON.parse(localStorage.getItem('__fsp_user'));

    if(path === '/auth') {
        store.state.isAuthScreen = true;
    } else {
        store.state.isAuthScreen = false;
    }

    if(to.matched.some(record => record.meta.requiresAdmin)) {
        user && user.isAdmin ? next() : next({ path: '/' });
    } else {
        next();
    }
});

export default router;
