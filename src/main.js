import Vue from 'vue';
import App from './App.vue';
import router from './config/router/router';
import store from './config/store/store';
import vuetify from './plugins/vuetify';

import firebase from 'firebase';
import 'firebase/firestore';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faEdit, faPlusCircle, faTrash, faFileAlt,
    faCheckCircle, faTimesCircle, faNewspaper, faCalendarDay,
    faMapMarkerAlt, faCog, faSignOutAlt, faSignInAlt,
    faSearch, faClock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon} from "@fortawesome/vue-fontawesome";

library.add(faHome, faEdit, faPlusCircle, faTrash,
    faFileAlt, faCheckCircle, faTimesCircle, faNewspaper,
    faCalendarDay, faMapMarkerAlt, faCog, faSignOutAlt,
    faSignInAlt, faSearch, faClock);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

firebase.initializeApp(require('../.env'));

export const db = firebase.firestore();
export const storage = firebase.storage();

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
